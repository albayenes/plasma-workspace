# Translation of plasma_runner_sessions to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2008, 2009, 2011.
msgid ""
msgstr ""
"Project-Id-Version: krunner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-06 01:40+0000\n"
"PO-Revision-Date: 2011-01-14 16:00+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: sessionrunner.cpp:20
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to log out of the "
"session"
msgid "logout;log out"
msgstr ""

#: sessionrunner.cpp:23
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Logger ut og avslutter denne skrivebordsøkta"

#: sessionrunner.cpp:26
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to shut down the "
"computer"
msgid "shutdown;shut down;power;power off"
msgstr ""

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "Slår av datamaskinen"

#: sessionrunner.cpp:32
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to restart the "
"computer"
msgid "restart;reboot"
msgstr ""

#: sessionrunner.cpp:35
#, kde-format
msgid "Reboots the computer"
msgstr "Starter maskinen på nytt"

#: sessionrunner.cpp:39
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to lock the screen"
msgid "lock;lock screen"
msgstr ""

#: sessionrunner.cpp:41
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Låser gjeldende økter og starter pauseskjermen"

#: sessionrunner.cpp:44
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to save the "
"desktop session"
msgid "save;save session"
msgstr ""

#: sessionrunner.cpp:47
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:50
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to switch user "
"sessions"
msgid "switch user;new session"
msgstr ""

#: sessionrunner.cpp:53
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Starer en ny økt som en annen bruker"

#: sessionrunner.cpp:56
#, kde-format
msgctxt "KRunner keyword to list user sessions"
msgid "sessions"
msgstr ""

#: sessionrunner.cpp:57
#, kde-format
msgid "Lists all sessions"
msgstr "Lister alle økter"

#: sessionrunner.cpp:60
#, kde-format
msgctxt "KRunner keyword to switch user sessions"
msgid "switch"
msgstr ""

#: sessionrunner.cpp:62
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Bytter til den aktive økta for bruker :q:, eller lister alle aktive økter "
"hvis :q: ikke er oppgitt"

#: sessionrunner.cpp:81
#, kde-format
msgctxt "log out command"
msgid "Log Out"
msgstr ""

#: sessionrunner.cpp:89
#, kde-format
msgctxt "turn off computer command"
msgid "Shut Down"
msgstr ""

#: sessionrunner.cpp:96
#, kde-format
msgctxt "restart computer command"
msgid "Restart"
msgstr ""

#: sessionrunner.cpp:103
#, kde-format
msgctxt "lock screen command"
msgid "Lock"
msgstr ""

#: sessionrunner.cpp:110
#, kde-format
msgid "Save Session"
msgstr ""

#: sessionrunner.cpp:151
#, kde-format
msgid "Switch User"
msgstr ""

#: sessionrunner.cpp:237
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type '%1')</li><li>Plasma widgets (such as the "
"application launcher)</li></ul>"
msgstr ""

#: sessionrunner.cpp:246
#, kde-format
msgid "New Desktop Session"
msgstr ""
