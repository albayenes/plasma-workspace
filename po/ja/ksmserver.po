# Translation of ksmserver into Japanese.
# This file is distributed under the same license as the kdebase package.
# Noboru Sinohara <shinobo@leo.bekkoame.ne.jp>, 2002.
# Ryuichi Kakuda <ryuk@user.script.nu>, 2004.
# Kenshi Muto <kmuto@debian.org>, 2004.
# Tadashi Jokagi <elf2000@users.sourceforge.net>, 2005.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2006, 2007.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2007, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2010-05-08 15:31-0700\n"
"Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: logout.cpp:273
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "ログアウトは %1 によってキャンセルされました"

#: main.cpp:67
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:71 main.cpp:80
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:74
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:82
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:87
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:90
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:104 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:106 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:116 main.cpp:130
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:119 main.cpp:133
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:150
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:153
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:160
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:194
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"KDE セッションマネージャは X11R6 標準セッション管理\n"
"プロトコル (XSMP) を利用します。"

#: main.cpp:198
#, kde-format
msgid "Restores the saved user session if available"
msgstr "可能なら保存されたセッションを復元"

#: main.cpp:201
#, kde-format
msgid "Also allow remote connections"
msgstr "リモート接続も許可する"

#: main.cpp:204
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:208
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:813
#, kde-format
msgctxt "@label an unknown executable is using resources"
msgid "[unknown]"
msgstr ""

#: server.cpp:836
#, kde-kuit-format
msgctxt "@label notification; %1 is a list of executables"
msgid ""
"Unable to manage some apps because the system's session management resources "
"are exhausted. Here are the top three consumers of session resources:\n"
"%1"
msgstr ""

#: server.cpp:1108
#, kde-kuit-format
msgctxt "@label notification; %1 is an executable name"
msgid ""
"Unable to restore <application>%1</application> because it is broken and has "
"exhausted the system's session restoration resources. Please report this to "
"the app's developers."
msgstr ""
