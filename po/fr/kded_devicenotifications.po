# SPDX-FileCopyrightText: 2023 Xavier Besnard <xavier.besnard@kde.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-19 01:58+0000\n"
"PO-Revision-Date: 2023-08-20 10:02+0200\n"
"Last-Translator: Xavier BESNARD <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.07.90\n"

#: devicenotifications.cpp:275
#, kde-format
msgid "%1 has been plugged in."
msgstr "%1 a été connecté."

#: devicenotifications.cpp:275
#, kde-format
msgid "A USB device has been plugged in."
msgstr "Un périphérique USB a été connecté."

#: devicenotifications.cpp:278
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "Périphérique USB détecté."

#: devicenotifications.cpp:295
#, kde-format
msgid "%1 has been unplugged."
msgstr "%1 a été débranché."

#: devicenotifications.cpp:295
#, kde-format
msgid "A USB device has been unplugged."
msgstr "Un périphérique USB a été débranché."

#: devicenotifications.cpp:298
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "Périphérique USB déconnecté"
