# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-19 01:53+0000\n"
"PO-Revision-Date: 2023-10-21 07:23+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "მთავარი"

#: contents/ui/ConfigGeneral.qml:33
#, kde-format
msgctxt ""
"Heading for a list of actions (leave, lock, switch user, hibernate, suspend)"
msgid "Show actions:"
msgstr "ქმედებების ჩვენება:"

#: contents/ui/ConfigGeneral.qml:34
#, kde-format
msgid "Log Out"
msgstr "გასვლა"

#: contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Shut Down"
msgstr "გამორთვა"

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Restart"
msgstr "გადატვირთვა"

#: contents/ui/ConfigGeneral.qml:53 contents/ui/data.js:5
#, kde-format
msgid "Lock"
msgstr "დაბლოკვა"

#: contents/ui/ConfigGeneral.qml:59
#, kde-format
msgid "Switch User"
msgstr "მომხმარებლის გადართვა"

#: contents/ui/ConfigGeneral.qml:65 contents/ui/data.js:46
#, kde-format
msgid "Hibernate"
msgstr "პროგრამული ძილი"

#: contents/ui/ConfigGeneral.qml:71 contents/ui/data.js:39
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "ძილი"

#: contents/ui/data.js:6
#, kde-format
msgid "Lock the screen"
msgstr "ეკრანის ჩაკეტვა"

#: contents/ui/data.js:12
#, kde-format
msgid "Switch user"
msgstr "მომხმარებლის გადართვა"

#: contents/ui/data.js:13
#, kde-format
msgid "Start a parallel session as a different user"
msgstr "სხვა მომხმარებლით პარალელური სესიის გაშვება"

#: contents/ui/data.js:18
#, kde-format
msgid "Shutdown…"
msgstr "გამორთვა…"

#: contents/ui/data.js:19
#, kde-format
msgid "Turn off the computer"
msgstr "კომპიუტერის გამორთვა"

#: contents/ui/data.js:25
#, kde-format
msgid "Restart…"
msgstr "გადატვირთვა…"

#: contents/ui/data.js:26
#, kde-format
msgid "Reboot the computer"
msgstr "კომპიუტერის გადატვირთვა"

#: contents/ui/data.js:32
#, kde-format
msgid "Logout…"
msgstr "გამოსვლა…"

#: contents/ui/data.js:33
#, kde-format
msgid "End the session"
msgstr "სესიის დასრულება"

#: contents/ui/data.js:40
#, kde-format
msgid "Sleep (suspend to RAM)"
msgstr "ძილი (RAM-ში)"

#: contents/ui/data.js:47
#, kde-format
msgid "Hibernate (suspend to disk)"
msgstr "პროგრამული ძილი (მეხსირების დისკზე დამახსოვრებით)"

#~ msgid "Logout"
#~ msgstr "გასვლა"

#~ msgid "Reboot"
#~ msgstr "გადატვირთვა"
