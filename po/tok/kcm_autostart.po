# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Janet Blackquill <uhhadd@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:39+0000\n"
"PO-Revision-Date: 2024-01-11 18:41-0500\n"
"Last-Translator: Janet Blackquill <uhhadd@gmail.com>\n"
"Language-Team: C <kde-i18n-doc@kde.org>\n"
"Language: tok\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n == 0 ? 0 : n == 1 ? 1 : n == 2 ? 2 : 3;\n"
"X-Generator: Lokalize 22.11.70\n"

#: autostartmodel.cpp:376
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "nasin URL \"%1\" li tan ala open."

#: autostartmodel.cpp:379
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1\" li lon ala."

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" li lipu ala."

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\" li ken ala lukin."

#: ui/entry.qml:52
#, kde-format
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "nimi ilo:"

#: ui/entry.qml:58
#, kde-format
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "pilin:"

#: ui/entry.qml:64
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr "tenpo open pini:"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr "o pini"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "o open"

#: ui/entry.qml:109
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr "mi ken ala lukin e toki ilo. o alasa sin."

#: ui/entry.qml:113
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr "o alasa sin"

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr "o ken e kepeken ilo"

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""
"sina wile kepeken e ona lon weka sina la lipu '%1' li wile ken pi kepeken "
"ilo."

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""
"sina wile kepeken e ona lon kama sina la lipu '%1' li wile ken pi kepeken "
"ilo."

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr "o kama e..."

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr "o kama e ilo..."

#: ui/main.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "o kama e ilo kama..."

#: ui/main.qml:79
#, kde-format
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "o kama e ilo weka..."

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""
"tenpo pini la %1 li kama ala open. sina sin e ilo sijelo sina la mi ken "
"lukin e sona open."

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr "kama ala open"

#: ui/main.qml:146
#, kde-format
msgctxt "@action:button"
msgid "See properties"
msgstr "ijo lipu"

#: ui/main.qml:157
#, kde-format
msgctxt "@action:button"
msgid "Remove entry"
msgstr "o weka e ni"

#: ui/main.qml:173
#, kde-format
msgid "Applications"
msgstr "ilo"

#: ui/main.qml:176
#, kde-format
msgid "Login Scripts"
msgstr "ilo kama"

#: ui/main.qml:179
#, kde-format
msgid "Pre-startup Scripts"
msgstr "ilo pi kama open"

#: ui/main.qml:182
#, kde-format
msgid "Logout Scripts"
msgstr "ilo weka"

#: ui/main.qml:191
#, kde-format
msgid "No user-specified autostart items"
msgstr "ilo open li lon ala."

#: ui/main.qml:192
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""
"sina wile lon e ilo open la o nena e <interface>o kama e...</interface>"

#: ui/main.qml:207
#, kde-format
msgid "Choose Login Script"
msgstr "o wile e ilo kama"

#: ui/main.qml:227
#, kde-format
msgid "Choose Logout Script"
msgstr "o wile e ilo weka"

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr "pali lon tenpo ni"

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr "pali ala lon tenpo ni"

#: unit.cpp:28
#, kde-format
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "kama open"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr "kama pini"

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr "pakala"

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr "alasa sona GetCall li pakala: %1"

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr "mi ken ala kama lukin e toki ilo"
